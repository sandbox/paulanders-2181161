This is a buttons set and icons for BUEditor with Markdown Editor module.

Set includes this buttons:

* h1
* h2
* h3
* h*
* strong
* em
* ins
* del
* ol
* ul
* code
* pre
* blockquote
* table
* img
* a
* a (without ancor)
* abr
* [footnote]
* br
* hr
* [teaser break]
* [help]
